import { Request, Response } from 'express'
import User, { IUser } from '../models/User'
import jwt from 'jsonwebtoken'

export const signup = async (req: Request, res: Response) => {
  //saving a new user
  type TUser = {username: string, email:string, password:string}
  let { username, email, password }: IUser = req.body


  const usuario: IUser = new User({
    username,
    email,
    password
  })
  usuario.password = usuario.encryptPassword( usuario.password )
  const saveUser = await usuario.save()

  //token
  const token: string = jwt.sign( {_id: saveUser._id}, process.env.TOKEN_SECRET || 'fake')

  res.header('auth-token', token).json({saveUser})
}

export const signin = async (req: Request, res: Response) => {
  
  let { email, password }: IUser = req.body
  const usuario = await User.findOne({email})
  if( !usuario ) return res.status(400).json({error: 'Not Found'})
  const correctPassword:boolean = usuario.validatePassword( password )
  if( !correctPassword ) return res.status(400).json({error: 'Invalid Password'})

  const token: string = jwt.sign( {_id: usuario._id}, process.env.TOKEN_SECRET || 'fake', {
    expiresIn: 60
  })

  res.header('auth-token', token).json(usuario)
}

export const profile = async (req: Request, res: Response) => {
  const usuario = await User.findById(req.userId)
  if(!usuario) return res.status(404).json('Not Found')

  res.json(usuario)
}
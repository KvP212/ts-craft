import {Schema, model, Document} from 'mongoose'
import bcrypt from 'bcryptjs'

export interface IUser extends Document{
  username: string;
  email: string;
  password: string;
  encryptPassword(password:string): string;
  validatePassword(password:string): boolean;
}

const userSchema = new Schema( {
  username: {
    type: String,
    required: true,
    min: 4,
    lowercase: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true
  }
})

userSchema.methods.encryptPassword = (password: string): string => {
  const salt:string = bcrypt.genSaltSync(10)
  return bcrypt.hashSync( password, salt)
}

userSchema.methods.validatePassword = function (password: string): boolean {
  return bcrypt.compareSync( password, this.password)
}

export default model<IUser>('User', userSchema)

import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost/ts-craft', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})
.then( db => console.log('DB Connect'))
.catch( err => console.log(err))
